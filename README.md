# cmd-fah-installer-wrapper

This is a wrapper for the [Folding@Home](https://folgingathome.org/) installer v7.5.1
The provided installer does not work for mass deployment as it has no command line options for silent configuration.
This is the result of this [thread](https://foldingforum.org/viewtopic.php?f=106&t=33127) on the folding forum.
  
# How to use:
Download cmd-fah-installer-wrapper.exe and the original installer from F@H and put them in the same directory. 
Then execute with your choice of command line options. (Tested on Win10)
Following options are available
```
cmd-fah-installer-wrapper.exe /S /user=yourUsername
```
(Default: Anonymous)

```
cmd-fah-installer-wrapper.exe /S /team=yourTeamNumber
```
(Default: 0)

```
cmd-fah-installer-wrapper.exe /S /passkey=yourPasskey
```
(Default: "")

```
cmd-fah-installer-wrapper.exe /S /allow=0.0.0.0/0 
```
(Default: 127.0.0.1) Restricts remote acces

```
cmd-fah-installer-wrapper.exe /S /password=yourPasswod
```
(Default: "") Your Passwort for remote access

# Example:
```
>  .\cmd-fah-installer-wrapper.exe /S /user=wuerfelfreak /team=233518 /passkey=000000000yourPasskey00000000 /allow=0.0.0.0/0 /password=yourPassword
```

# Further information
Registry-Key ```HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\FAHClient``` will be set to ```C:\Program Files (x86)\FAHClient```
The configuration-file will look like this
```xml
<config>
    <allow v='123.456.789.0/0'/>
    <password v='yourPasswor'/>
    <power v='full'/>
    <passkey v='000000000yourPasskey00000000'/>
    <team v='0'/>
    <user v='username'/>
    <slot id='0' type='CPU'/>
</config>
```
and will be saved in C:\Program Files (x86)\FAHClient\config.xml

Also a new system service will be created to start fah at boottime:
Name: Folding@Home Client
BinaryPath: ```C:\Program Files (x86)\FAHClient\FAHClient.exe```


# Warning!
I have absolutely no experience with windwos installers. So do NOT use this without checking it first! 

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

